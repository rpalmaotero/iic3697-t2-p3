import tensorflow as tf

"""
TODO:
- agregar summaries.

OJO:
- estados iniciales de las recurrencias.
- inicializaciones de los parámetros.
- activaciones.
- número de parámetros.
- grado que afecta output dinámico, evaluar hacer
    clasificación sobre respuestas.
"""


NORMAL_INITIALIZER = tf.random_normal_initializer(mean=0, stddev=1)


def define_model_flags(flags):
    flags.DEFINE_integer(
        "embeddings_size", 300,
        """Embeddings dimension.""")
    flags.DEFINE_integer(
        "contexts_length", 44,
        """Maximum length of the contexts.""")
    flags.DEFINE_integer(
        "input_lstm_units", 300,
        """Number of units of the input LSTM.""")
    flags.DEFINE_integer(
        "intermediate_representation_size", 500,
        """Size of the intermediate representation of the context and question.""")
    flags.DEFINE_integer(
        "answers_vocab_size", 76945,
        """Size of the answers vocabulary.""")


def _get_variable(name, shape, initializer=None):
    var = tf.get_variable(name, shape=shape, initializer=initializer)
    return var

def _input_module(context, question, flags):
    """Función que construye el grafo del módulo de entrada.
    Args:
        - context: tensor de tamaño [BATCH_SIZE, CONTEXT_LENGTH, EMBEDDING_SIZE].
        - question: tensor de tamaño [BATCH_SIZE, EMBEDDING_SIZE].
    Returns:
        - question_knowledge_base: tensor de tamaño [BATCH_SIZE, INTERMEDIATE_REPRESENTATION_SIZE].
    """
    INPUT_MODULE_LSTM_UNITS = flags.input_lstm_units
    EMBEDDING_SIZE = flags.embeddings_size
    CONTEXT_LENGTH = flags.contexts_length
    INTERMEDIATE_REPRESENTATION_SIZE = flags.intermediate_representation_size

    with tf.variable_scope("input_module"):
        input_lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(
            num_units=INPUT_MODULE_LSTM_UNITS,
            state_is_tuple=True
        )
        input_states_series, _ = tf.nn.dynamic_rnn( # [BATCH_SIZE, CONTEXT_LENGTH, INPUT_MODULE_LSTM_UNITS]
            cell=input_lstm_cell,
            inputs=context,
            dtype=tf.float32
        ) 
        question_reshaped = tf.reshape( # [BATCH_SIZE, 1, EMBEDDING_SIZE]
            question,
            shape=[-1, 1, EMBEDDING_SIZE]
        )
        question_context_product = tf.multiply( # [BATCH_SIZE, CONTEXT_LENGTH, EMBEDDING_SIZE]
            context,
            question_reshaped
        ) 
        question_context_sum = tf.reduce_sum( # [BATCH_SIZE, CONTEXT_LENGTH]
            question_context_product,
            axis=2
        ) 
        question_context_p_scores = tf.nn.softmax( # [BATCH_SIZE, CONTEXT_LENGTH]
            question_context_sum,
            dim=1
        ) 
        question_context_p_scores_reshaped = tf.reshape( # [BATCH_SIZE, CONTEXT_LENGTH, 1]
            question_context_p_scores,
            shape=[-1, CONTEXT_LENGTH, 1]
        )
        input_states_attended = tf.multiply( # [BATCH_SIZE, CONTEXT_LENGTH, EMBEDDING_SIZE]
            input_states_series,
            question_context_p_scores_reshaped 
        )
        knowledge_base = tf.reduce_sum( # [BATCH_SIZE, EMBEDDING_SIZE]
            input_states_attended,
            axis=1
        ) 
        question_knowledge_base_concat = tf.concat( # [BATCH_SIZE, 2 * EMBEDDING_SIZE]
            [question, knowledge_base],
            axis=1
        )
        M_projection_matrix = _get_variable(
            "M_matrix",
            shape=[2 * EMBEDDING_SIZE, INTERMEDIATE_REPRESENTATION_SIZE],
            initializer=NORMAL_INITIALIZER
        )
        question_knowledge_base = tf.matmul( # [BATCH_SIZE, INTERMEDIATE_SIZE]
            question_knowledge_base_concat,
            M_projection_matrix
        )
    return question_knowledge_base


def _output_module(intermediate_repr, flags):
    """Función que construye el grafo del módulo de salida.
    Args:
        - intermediate_repr: tensor de tamaño [BATCH_SIZE, INTERMEDIATE_SIZE].
    Returns:
        - logits: tensor de tamaño [BATCH_SIZE, ANSWER_VOCAB_SIZE].
    """
    INTERMEDIATE_REPRESENTATION_SIZE = flags.intermediate_representation_size
    ANSWER_VOCAB_SIZE = flags.answers_vocab_size

    with tf.variable_scope("output_module"):
        N_decoder_matrix = _get_variable(
            "N_matrix",
            shape=[INTERMEDIATE_REPRESENTATION_SIZE, ANSWER_VOCAB_SIZE]
        )
        logits = tf.matmul(intermediate_repr, N_decoder_matrix)
    return logits


def inference(context, question):
    """Función que realiza la inferencia.
    Args:
        - context: tensor de tamaño [BATCH_SIZE, CONTEXT_LENGTH, EMBEDDING_SIZE].
        - question: tensor de tamaño [BATCH_SIZE, EMBEDDING_SIZE].
    Returns:
        - logits: tensor de tamaño [BATCH_SIZE, ANSWER_VOCAB_SIZE]
    """
    flags = tf.app.flags.FLAGS
    intermediate_repr = _input_module( # [BATCH_SIZE, INTERMEDIATE_SIZE]
        context,
        question,
        flags
    )
    logits = _output_module( # [BATCH_SIZE, ANSWER_VOCAB_SIZE]
        intermediate_repr,
        flags
    )
    return logits


def loss(logits, answers):
    """Función que calcula la pérdida.
    Args:
        - logits: tensor de tamaño [BATCH_SIZE, ANSWER_VOCAB_SIZE].
        - answers: tensor de tamaño [BATCH_SIZE].
    Returns:
        - loss: tensor de 1-D con la pérdida.
    """
    losses = tf.nn.sparse_softmax_cross_entropy_with_logits( # [BATCH_SIZE]
        labels=answers,
        logits=logits
    )
    loss = tf.reduce_mean(losses)
    return loss


def accuracy(logits, answers):
    """Función que calcula la precisión.
    Args:
        - logits: tensor de tamaño [BATCH_SIZE, ANSWER_VOCAB_SIZE].
        - answers: tensor de tamaño [BATCH_SIZE].
    Returns:
        - accuracy: tensor de 1-D con la precisión.
    """
    predictions = tf.argmax(logits, axis=1) # [BATCH_SIZE]
    correct_predictions = tf.equal(answers, predictions) # [BATCH_SIZE]
    accuracy = tf.reduce_mean(
        tf.cast(correct_predictions, tf.float32)
    )
    return accuracy
