import io
import re

import numpy as np
from tqdm import tqdm


def tokenize(sentence):
    """
    Maps a sentence to a list of tokens. For example:
        'Mary moved to the bathroom.' => ['mary', 'moved', 'to', 'the', 'bathroom', '.']
        'Where is Mary?' => ['where', 'is', 'mary', '?']
    """
    return [token.strip().lower() for token in re.split(r"(\W+)?", sentence) if token.strip()]


def load_fasttext_vectors(vectors_path):
    # ref: https://github.com/facebookresearch/fastText/blob/master/docs/english-vectors.md
    fin = io.open(vectors_path, 'r', encoding='utf-8', newline='\n', errors='ignore')
    n, d = map(int, fin.readline().split())
    print("fastText embeddings: %d\tfastText embedding size: %d" % (n, d))
    data = {}
    for line in tqdm(fin, total=n):
        tokens = line.rstrip().split(' ')
        data[tokens[0]] = np.array(list(map(float, tokens[1:])))
    return data

