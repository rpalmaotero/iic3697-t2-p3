import os
import json
import pickle
import random
import math

import numpy as np
from tqdm import tqdm
import tensorflow as tf

from input import read_metadata_path, data_generator, val_data_generator
from model import inference, loss, accuracy, define_model_flags


flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_integer(
    "batch_size", 64, 
    """Batch size.""")
flags.DEFINE_integer(
    "epochs", 400,
    """Number of epochs to train.""")
flags.DEFINE_integer(
    "validate_every_n_epochs", 1,
    """Number of epochs to perform a validation.""")
flags.DEFINE_float(
    "learning_rate", 0.01,
    """Learning rate to apply in each training iteration.""")
flags.DEFINE_bool(
    "use_learning_rate_decay", False,
    """Indicates if the learning rate should decay or not.""")
flags.DEFINE_float(
    "decay_steps", 2000,
    """Number of batches between each learning rate decayment.""")
flags.DEFINE_float(
    "decay_rate", 0.8,
    """A factor of how much to decay the learning rate.""")
flags.DEFINE_string(
    "data_dir_path", "/Users/rodolfo/U/IIC3697.Deep.Learning/Tareas/T2/datasets/SQuAD", 
    """Path to the directory where dataset is located.""")
flags.DEFINE_string(
    "metadata_file_name", "metadata.json",
    """Name of the JSON file that includes the dataset metadata.""")
flags.DEFINE_string(
    "train_dir_name", "train_tokenized",
    """Name of the directory that contains the training tokenized data.""")
flags.DEFINE_string(
    "val_dir_name", "dev_tokenized",
    """Name of the directory that contains the validation tokenized data.""")
flags.DEFINE_string(
    "tensorboard_log_dir", "/tmp/iic3697-t2-p3/last_run/",
    """Path to the directory where the Tensorboard checkpoints will be stored.""")
flags.DEFINE_string(
    "checkpoints_target", "/tmp/iic3697-t2-p3/model.ckpt",
    """Path to the folder where the checkpoints are going to be stored.""")
define_model_flags(flags)


def train(argv=None):
    # Read the training and validation data.
    training_data_path = os.path.join(
        FLAGS.data_dir_path,
        FLAGS.train_dir_name
    )
    training_data_generator = data_generator(
        folder_path=training_data_path,
        batch_size=FLAGS.batch_size,
        epochs=FLAGS.epochs
    )
    validation_data_path = os.path.join(
        FLAGS.data_dir_path,
        FLAGS.val_dir_name
    )
    validation_data_generator = val_data_generator(
        folder_path=validation_data_path,
        batch_size=FLAGS.batch_size
    )

    # Create the placeholders
    contexts = tf.placeholder(
        tf.float32,
        shape=[FLAGS.batch_size, FLAGS.contexts_length, FLAGS.embeddings_size],
        name="contexts"
    )
    questions = tf.placeholder(
        tf.float32,
        shape=[FLAGS.batch_size, FLAGS.embeddings_size],
        name="questions"
    )
    answers = tf.placeholder(
        tf.int64,
        shape=[FLAGS.batch_size],
        name="answers"
    )

    # Inference operation
    logits = inference(contexts, questions)

    # Loss operation
    loss_op = loss(logits, answers)
    tf.summary.scalar("loss", loss_op)

    # Accuracy operation
    accuracy_op = accuracy(logits, answers)
    tf.summary.scalar("accuracy", accuracy_op)

    # Training operation
    global_step = tf.Variable(0, name="global_step", trainable=False)
    increment_global_step_op = tf.assign_add(global_step, 1, name="increment_global_step_op")
    if FLAGS.use_learning_rate_decay:
        learning_rate = tf.train.exponential_decay(
            learning_rate=FLAGS.learning_rate,
            global_step=global_step,
            decay_steps=FLAGS.decay_steps,
            decay_rate=FLAGS.decay_steps
        )
    else:
        learning_rate = FLAGS.learning_rate
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(loss_op)

    # Init ops
    global_init_op = tf.global_variables_initializer()
    local_init_op = tf.local_variables_initializer()

    # Add ops to save and restore all the variables.
    saver = tf.train.Saver()

    with tf.Session() as sess:
        sess.run(global_init_op)
        sess.run(local_init_op)

        # Merge summaries.
        train_writer_path = os.path.join(FLAGS.tensorboard_log_dir, "train")
        val_writer_path = os.path.join(FLAGS.tensorboard_log_dir, "validation")
        train_writer = tf.summary.FileWriter(train_writer_path)
        val_writer = tf.summary.FileWriter(val_writer_path)
        summaries_op = tf.summary.merge_all()
        train_writer.add_graph(sess.graph)

        batches_per_epoch, epochs = next(training_data_generator)
        val_batches_per_epoch = next(validation_data_generator)
        for i_epoch in range(epochs):
            avg_cost = 0
            avg_accur = 0
            for j_batch in tqdm(range(batches_per_epoch)):
                current_batch_data = next(training_data_generator)
                cost, accur, summaries, _ = sess.run([loss_op, accuracy_op, summaries_op, train_op], feed_dict={
                    contexts: current_batch_data[0],
                    questions: current_batch_data[1],
                    answers: current_batch_data[2]
                })
                # Guard against numerical instability.
                assert not math.isnan(cost)
                assert not math.isnan(accur)
                # Keep track of the average loss and accuracy.
                avg_cost += cost / batches_per_epoch
                avg_accur += accur / batches_per_epoch
                # Write summaries.
                train_writer.add_summary(summaries, i_epoch * batches_per_epoch + j_batch)
                sess.run(increment_global_step_op)

            saver.save(sess, FLAGS.checkpoints_target, global_step=global_step)
            print("Epoch: %d\tAverage loss: %f\tAverage accuracy: %f" % (i_epoch + 1, avg_cost, avg_accur))

            # Validation
            if i_epoch % FLAGS.validate_every_n_epochs == 0:
                print("Evaluating the validation performance...")
                avg_cost = 0
                avg_accur = 0
                for j_batch in tqdm(range(val_batches_per_epoch)):
                    current_batch_data = next(validation_data_generator)
                    cost, accur, summaries = sess.run([loss_op, accuracy_op, summaries_op], feed_dict={
                        contexts: current_batch_data[0],
                        questions: current_batch_data[1],
                        answers: current_batch_data[2]
                    })
                    # Guard against numerical instability.
                    assert not math.isnan(cost)
                    assert not math.isnan(accur)
                    # Keep track of the average loss and accuracy.
                    avg_cost += cost / batches_per_epoch
                    avg_accur += accur / batches_per_epoch
                # Write summaries.
                val_writer.add_summary(summaries, (i_epoch + 1) * batches_per_epoch)
                print("Validation loss: %f\tValidation accuracy: %f" % (avg_cost, avg_accur))

if __name__ == "__main__":
    tf.app.run(train)