import io
import os
import json
import pickle

import numpy as np
from tqdm import tqdm

from utils import tokenize, load_fasttext_vectors

DATASET_PATH = os.environ.get("DATASET_PATH", "/Users/rodolfo/U/IIC3697.Deep.Learning/Tareas/T2/datasets/SQuAD")
TRAIN_FILE_NAME = "train-v1.1.json"
TRAIN_TARGET_FOLDER = "train_tokenized"
DEV_FILE_NAME = "dev-v1.1.json"
DEV_TARGET_FOLDER = "dev_tokenized"
VOCAB_TARGET_FILE = "metadata.json"

FASTTEXT_EMBEDDINGS = os.environ.get("FASTTEXT_EMBEDDINGS", "/Users/rodolfo/U/IIC3697.Deep.Learning/Tareas/T2/datasets/wiki-news-300d-1M.vec")
FASTTEXT_EMBEDDINGS_SIZE = 300
_FASTTEXT_TOTAL_COUNT = 0
_FASTTEXT_MISS_COUNT = 0
print("Loading fastText model, might take a while...")
FASTTEXT_MODEL = load_fasttext_vectors(FASTTEXT_EMBEDDINGS)

PAD_TOKEN = "_PAD"
STOP_TOKEN = "_STOP"


class SQuADQuery:
    """Clase que representa una consulta sobre el set
        de datos SQuAD.
    """
    def __init__(self, id_, context, question, answer):
        """
        Args:
            - id_: identificador de la consulta.
            - context: lista de lista de tokens que consisten
                en el contexto de la consulta.
            - question: lista de tokens que consisten en la
                consulta.
            - answer: lista de tokens que consisten en la
                respuesta de la consulta.
        """
        self.id_ = id_
        self.context = context
        self.question = question
        self.answer = answer


def get_empty_embedding():
    """Función que retorna un embedding vacío.
    Args:
        - None.
    Returns:
        - empty embedding.
    """
    return np.zeros(FASTTEXT_EMBEDDINGS_SIZE)


def tokens_to_fasttext(tokens):
    """Función que calcula la representación de una lista
        de tokens mediante promedio ponderado. Se usan
        codificaciones obtenidas desde un modelo fastText.
    Args:
        - tokens: lista de tokens.
    Returns:
        - repr_: representación de la lista de tokens.
    """
    global _FASTTEXT_TOTAL_COUNT, _FASTTEXT_MISS_COUNT
    repr_ = get_empty_embedding()
    count = 0
    for token in tokens:
        _FASTTEXT_TOTAL_COUNT += 1
        try:
            repr_ += FASTTEXT_MODEL[token]
            count += 1
        except KeyError:
            _FASTTEXT_MISS_COUNT += 1
            continue
    if count > 0:
        repr_ = repr_ / count
    return repr_


def preprocess_squad_json(file_contents):
    """Función que realizar preprocesamiento al contenido
        de algún archivo JSON del set de datos SQuAD.
    Args:
        - file_contents: objeto con el contenido del
            archivo.
    Returns:
        - queries: lista de SQuADQueries.
        - vocab: vocabulario de posibles respuestas.
    """
    file_data = file_contents["data"]
    vocab = set()
    queries = []
    for topic in tqdm(file_data):
        topic_paragraphs = topic["paragraphs"]
        for paragraph in topic_paragraphs:
            paragraph_context = paragraph["context"]
            paragraph_qas = paragraph["qas"]
            sentences = paragraph_context.split(".")[:-1]
            sentences_tokenized = list(map(tokenize, sentences))
            sentences_fasttext = list(map(tokens_to_fasttext, sentences_tokenized))
            for qa in paragraph_qas:
                qa_question = qa["question"]
                qa_id = qa["id"]
                question_tokenized = tokenize(qa_question)
                question_fasttext = tokens_to_fasttext(question_tokenized)
                qa_answers = set(map(lambda a: a["text"].lower(), qa["answers"]))
                for answer in qa_answers:
                    query = SQuADQuery(
                        id_=qa_id,
                        context=sentences_fasttext,
                        question=question_fasttext,
                        answer=answer
                    )
                    queries.append(query)
                    vocab.update([answer])
    return queries, vocab


def get_token_map(vocab):
    """Función que construye un token map. Un token map
        consiste en un diccionario que mapea tokens a
        identificadores únicos. 
    Args:
        - vocab: conjunto de palabras en el vocabulario.
    Return:
        - token_map: diccionario que mapea tokens a identificadores.
    """
    tokens = sorted(list(vocab))
    token_map = {token: i for i, token in enumerate(tokens)}
    return token_map


def get_max_lengths(queries):
    """Función que retorna los largos máximos del dataset.
    Args:
        - queries: lista de SQuADQueries.
    Returns:
        - max_context_length: largo máximo de los contextos.
    """
    context_lengths = [len(query.context) for query in queries]
    return max(context_lengths)


def pad_queries(train_queries, dev_queries):
    """Función que rellena los contextos y respuestas del
        dataset hasta sus largos máximos.
    Args:
        - train_queries: lista de SQuADQueries de train.
        - dev_queries: lista de SQuADQueries de dev.
    Returns:
        - train_queries: lista de SQuADQueries de train rellenadas.
        - dev_queries: lista de SQuADQueries de dev rellenadas.
        - max_context_len: largo máximo de los contextos.
    """
    queries = train_queries + dev_queries
    max_context_len = get_max_lengths(queries)

    print("Max context length: %d" % max_context_len)

    print("Padding queries...")
    for query in tqdm(queries):
        for _ in range(max_context_len - len(query.context)):
            query.context.append(get_empty_embedding())
    return train_queries, dev_queries, max_context_len


def token_to_id(train_queries, dev_queries, token_map):
    """Función que mapea los tokens a identificadores únicos
        de un token_map.
    Args:
        - train_queries: lista de SQuADQueries de train.
        - dev_queries: lista de SQuADQueries de dev.
        - token_map: diccionario de mapeo entre tokens e
            identificadores.
    Returns:
        - train_queries: lista de SQuADQueries de train.
        - dev_queries: lista de SQuADQueries de dev.
    """
    queries = train_queries + dev_queries
    print("Mapping tokens to ids...")
    for query in tqdm(queries):
        query.answer = token_map[query.answer]
    return train_queries, dev_queries


def dump_queries(train_queries, dev_queries):
    """Función que guarda las queries preprocesadas en
        archivos con extensión .bin.
    Args:
        - train_queries: lista de SQuADQueries de train.
        - dev_queries: lista de SQuADQueries de dev.
    Returns:
        - None.
    """
    targets = [
        (TRAIN_TARGET_FOLDER, train_queries),
        (DEV_TARGET_FOLDER, dev_queries)
    ]
    for target_folder, queries in targets:
        print("Dumping %s..." % target_folder)
        target_folder_path = os.path.join(DATASET_PATH, target_folder)
        os.makedirs(target_folder_path, exist_ok=True)
        for query in tqdm(queries):
            query_path = os.path.join(target_folder_path, query.id_ + ".bin")
            query_dump = {
                "context": np.stack(query.context),
                "question": query.question,
                "answer": np.array(query.answer)
            }
            with open(query_path, "wb") as query_file:
                pickle.dump(query_dump, query_file)


def dump_vocab(vocab, token_map, max_context_len):
    """Función que guarda el vocabulario de las respuestas en
        archivos con extensión .json
    Args:
        - vocab: conjunto de palabras del vocabulario de respuestas.
        - token_map: diccionario que mapea tokens a identificadores.
        - max_context_len: largo máximo de los contextos.
    Returns:
        - None.
    """
    vocab_dump = {
        "vocab_size": len(vocab),
        "token_map": token_map,
        "max_context_len": max_context_len,
        "embeddings_size": FASTTEXT_EMBEDDINGS_SIZE
    }
    vocab_path = os.path.join(DATASET_PATH, VOCAB_TARGET_FILE)
    with open(vocab_path, "w") as vocab_file:
        json.dump(vocab_dump, vocab_file)


def preprocess_squad_dataset(dataset_path):
    """Función que realiza un preprocesamiento del
        set de datos SQuAD.
    Args:
        - dataset_path: string con el path al dataset.
    Returns:
        - None
    """
    train_data_path = os.path.join(dataset_path, TRAIN_FILE_NAME)
    dev_data_path = os.path.join(dataset_path, DEV_FILE_NAME)
    with open(train_data_path) as train_data_file:
        train_file_contents = json.load(train_data_file)
    with open(dev_data_path) as dev_data_file:
        dev_file_contents = json.load(dev_data_file)
    print("Preprocessing %s..." % TRAIN_FILE_NAME)
    train_queries, train_vocab = preprocess_squad_json(train_file_contents)
    print("Preprocessing %s..." % DEV_FILE_NAME)
    dev_queries, dev_vocab = preprocess_squad_json(dev_file_contents)
    vocab = train_vocab.union(dev_vocab)
    token_map = get_token_map(vocab)

    print("Training queries: %d" % len(train_queries))
    print("Dev queries: %d" % len(dev_queries))
    print("Answer vocabulary size: %d" % len(vocab))
    print("fastText miss percentage: %.4f" % ((_FASTTEXT_MISS_COUNT * 100) / _FASTTEXT_TOTAL_COUNT))

    train_queries, dev_queries, max_context_len \
        = pad_queries(train_queries, dev_queries)
    train_queries, dev_queries = token_to_id(train_queries, dev_queries, token_map)

    dump_queries(train_queries, dev_queries)
    dump_vocab(vocab, token_map, max_context_len)

if __name__ == "__main__":
    preprocess_squad_dataset(DATASET_PATH)