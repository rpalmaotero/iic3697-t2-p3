import os
import json
import pickle
import random
import math

import numpy as np
from tqdm import tqdm
import tensorflow as tf

from input import read_metadata_path, val_data_generator
from model import inference, loss, accuracy, define_model_flags


flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_integer(
    "batch_size", 64, 
    """Batch size.""")
flags.DEFINE_string(
    "data_dir_path", "/Users/rodolfo/U/IIC3697.Deep.Learning/Tareas/T2/datasets/SQuAD", 
    """Path to the directory where dataset is located.""")
flags.DEFINE_string(
    "metadata_file_name", "metadata.json",
    """Name of the JSON file that includes the dataset metadata.""")
flags.DEFINE_string(
    "val_dir_name", "dev_tokenized",
    """Name of the directory that contains the validation tokenized data.""")
flags.DEFINE_string(
    "checkpoints_source", "/tmp/iic3697-t2-p3/",
    """Path to the folder where the checkpoints are stored.""")
flags.DEFINE_string(
    "dump_filename", "eval.json",
    """Name of the file with the predictions.""")
define_model_flags(flags)


def eval_(argv=None):
    # Read the eval data.
    validation_data_path = os.path.join(
        FLAGS.data_dir_path,
        FLAGS.val_dir_name
    )
    validation_data_generator = val_data_generator(
        folder_path=validation_data_path,
        batch_size=FLAGS.batch_size
    )

    # Read the metadata
    metadata_path = os.path.join(FLAGS.data_dir_path, FLAGS.metadata_file_name)
    with open(metadata_path) as metadata_file:
        metadata = json.load(metadata_file)
    id_to_token = { val: key for key, val in metadata["token_map"].items() }

    # Create the placeholders
    contexts = tf.placeholder(
        tf.float32,
        shape=[FLAGS.batch_size, FLAGS.contexts_length, FLAGS.embeddings_size],
        name="contexts"
    )
    questions = tf.placeholder(
        tf.float32,
        shape=[FLAGS.batch_size, FLAGS.embeddings_size],
        name="questions"
    )
    answers = tf.placeholder(
        tf.int64,
        shape=[FLAGS.batch_size],
        name="answers"
    )

    # Inference operation
    logits = inference(contexts, questions)

    # Loss operation
    loss_op = loss(logits, answers)

    # Accuracy operation
    accuracy_op = accuracy(logits, answers)

    # Predictions.
    all_predictions = {}

    with tf.Session() as sess:
        # Restore variables from disk.
        saver = tf.train.Saver()
        latest_checkpoint = tf.train.latest_checkpoint(FLAGS.checkpoints_source)
        saver.restore(sess, latest_checkpoint)

        val_batches_per_epoch = next(validation_data_generator)
        print("Evaluating the model performance...")
        avg_cost = 0
        avg_accur = 0
        for _ in tqdm(range(val_batches_per_epoch)):
            current_batch_data = next(validation_data_generator)
            cost, accur, logits_predicted = sess.run([loss_op, accuracy_op, logits], feed_dict={
                contexts: current_batch_data[0],
                questions: current_batch_data[1],
                answers: current_batch_data[2]
            })
            # Guard against numerical instability.
            assert not math.isnan(cost)
            assert not math.isnan(accur)
            # Keep track of the average loss and accuracy.
            avg_cost += cost / val_batches_per_epoch
            avg_accur += accur / val_batches_per_epoch
            # Store the predictions.
            for filename, predictions in zip(current_batch_data[3], logits_predicted):
                file_id = filename.split(".")[-2]
                answer_id = np.argmax(predictions)
                all_predictions[file_id] = id_to_token[answer_id]

        print("Validation loss: %f\tValidation accuracy: %f" % (avg_cost, avg_accur))
    
    # Dump the predictions.
    with open(FLAGS.dump_filename, "w") as dump_file:
        json.dump(all_predictions, dump_file)

if __name__ == "__main__":
    tf.app.run(eval_)