import os
import json
import pickle
import random

import numpy as np

def read_metadata_path(metadata_path):
    with open(metadata_path) as metadata_file:
        metadata = json.load(metadata_file)
    return metadata


def _read_squad_tokenized_filenames(folder_path, filenames):
    file_paths = list(map(
        lambda name: os.path.join(folder_path, name),
        filenames
    ))
    contexts = []
    questions = []
    answers = []
    for file_path in file_paths:
        with open(file_path, "rb") as file_object:
            file_contents = pickle.load(file_object)
        contexts.append(file_contents["context"])
        questions.append(file_contents["question"])
        answers.append(file_contents["answer"])
    return np.stack(contexts), np.stack(questions), np.array(answers)


def data_generator(folder_path, batch_size, epochs):
    """Función generadora de los datos para los distintos batches.
    Args:
        - folder_path: path al directorio que contiene al dataset.
        - batch_size: cantidad de elementos a entregar en cada batch.
        - epochs: número de épocas que se debe recorrer el dataset.
    Yields:
        - contexts: numpy array de dimensión [BATCH_SIZE, MAX_CONTEXT_LENGTH, EMBEDDING_SIZE].
        - questions: numpy array de dimensión [BATCH_SIZE, EMBEDDING_SIZE].
        - answers: numpy array de dimensión [BATCH_SIZE].
    """
    filenames = list(filter(
        lambda path: path.endswith(".bin"),
        os.listdir(folder_path)
    ))
    filenames_length = len(filenames)
    batches_per_epoch = filenames_length // batch_size
    yield batches_per_epoch, epochs
    for _ in range(epochs):
        random.shuffle(filenames)
        for j_batch in range(batches_per_epoch):
            slice_start = j_batch * batch_size
            slice_end = (j_batch + 1) * batch_size
            batch_filenames = filenames[slice_start:slice_end]
            contexts, questions, answers = _read_squad_tokenized_filenames(
                folder_path,
                batch_filenames
            )
            yield contexts, questions, answers

def val_data_generator(folder_path, batch_size):
    """Función generadora de los datos de validación.
    Args:
        - folder_path: path al directorio que contiene al dataset.
        - batch_size: cantidad de elementos a entregar en cada batch.
        - epochs: número de épocas que se debe recorrer el dataset.
    Yields:
        - contexts: numpy array de dimensión [BATCH_SIZE, MAX_CONTEXT_LENGTH, EMBEDDING_SIZE].
        - questions: numpy array de dimensión [BATCH_SIZE, EMBEDDING_SIZE].
        - answers: numpy array de dimensión [BATCH_SIZE].
        - batch_filenames: lista de largo BATCH_SIZE.
    """
    filenames = list(filter(
        lambda path: path.endswith(".bin"),
        os.listdir(folder_path)
    ))
    filenames_length = len(filenames)
    batches_per_epoch = filenames_length // batch_size
    yield batches_per_epoch
    while True:
        random.shuffle(filenames)
        for j_batch in range(batches_per_epoch):
            slice_start = j_batch * batch_size
            slice_end = (j_batch + 1) * batch_size
            batch_filenames = filenames[slice_start:slice_end]
            contexts, questions, answers = _read_squad_tokenized_filenames(
                folder_path,
                batch_filenames
            )
            yield contexts, questions, answers, batch_filenames